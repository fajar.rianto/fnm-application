package com.example.fnm.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.fnm.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TableCalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TableCalFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public TableCalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TableCalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TableCalFragment newInstance(String param1, String param2) {
        TableCalFragment fragment = new TableCalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_table_cal, container, false);
        Spinner listItem = view.findViewById(R.id.listItem);
        ImageView buah = view.findViewById(R.id.buah);
        ImageView lauk = view.findViewById(R.id.lauk);
        ImageView siapsaji = view.findViewById(R.id.siapsaji);
        ImageView pokok = view.findViewById(R.id.pokok);
        ImageView sayur = view.findViewById(R.id.sayur);

        ImageView[] imageViews = {buah, lauk, siapsaji, pokok, sayur};

        listItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (int i=0; i< imageViews.length; i++){
                    if (i != position){
                        imageViews[i].setVisibility(View.GONE);
                    }else{
                        imageViews[i].setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }
}