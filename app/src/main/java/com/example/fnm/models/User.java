package com.example.fnm.models;

public class User {

    private String id;
    private String username;
    private String email;
    private String phone;
    private String jk;
    private String jumlahkalori;
    private String beratideal;
    private String jumlahlangkah;

    public User() {
    }

    public User(String id, String username, String email, String phone, String jk, String jumlahkalori, String beratideal, String jumlahlangkah) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.jk = jk;
        this.jumlahkalori = jumlahkalori;
        this.beratideal = beratideal;
        this.jumlahlangkah = jumlahlangkah;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getJumlahkalori() {
        return jumlahkalori;
    }

    public void setJumlahkalori(String jumlahkalori) {
        this.jumlahkalori = jumlahkalori;
    }

    public String getBeratideal() {
        return beratideal;
    }

    public void setBeratideal(String beratideal) {
        this.beratideal = beratideal;
    }

    public String getJumlahlangkah() {
        return jumlahlangkah;
    }

    public void setJumlahlangkah(String jumlahlangkah) {
        this.jumlahlangkah = jumlahlangkah;
    }
}
