package com.example.fnm.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import com.example.fnm.R;
import com.example.fnm.tools.Constraint;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText username;
    EditText email;
    EditText phone;
    Spinner gender;
    EditText password;
    EditText confpassword;
    TextView login;
    Button btnRegister;
    LinearLayout lin;
    LottieAnimationView lottieAnimationView;

    DatabaseReference databaseReference;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference("Users");

        lin = findViewById(R.id.lin);
        lottieAnimationView = findViewById(R.id.lottie);
        username = findViewById(R.id.et_username);
        email = findViewById(R.id.et_email);
        phone = findViewById(R.id.et_phone);
        gender = findViewById(R.id.listItem);
        password = findViewById(R.id.et_password);
        confpassword = findViewById(R.id.et_confpassword);
        login = findViewById(R.id.login);
        btnRegister = findViewById(R.id.btnDaftar);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()){
                    lin.setVisibility(View.GONE);
                    lottieAnimationView.setVisibility(View.VISIBLE);
                    auth.createUserWithEmailAndPassword(email.getText().toString().trim(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            lin.setVisibility(View.VISIBLE);
                            lottieAnimationView.setVisibility(View.GONE);
                            if (task.isSuccessful()){
//
                                FirebaseFirestore db = FirebaseFirestore.getInstance();
                                Map<String, Object> user = new HashMap<>();
                                user.put("username", username.getText().toString());
                                user.put("email", email.getText().toString());
                                user.put("phone", phone.getText().toString());
                                user.put("gender", gender.getSelectedItem().toString());
                                user.put("jumlahkalori", "-");
                                user.put("beratideal", "-");
                                user.put("jumlahlangkah", "-");

                                db.collection("users")
                                        .add(user)
                                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {

                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {

                                            }
                                        });

                                Toast.makeText(getApplicationContext(), Constraint.SUCCESS,Toast.LENGTH_LONG).show();
                                finish();
                            }else{
                                Toast.makeText(getApplicationContext(), Constraint.FAILEDREGISTER,Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private boolean check(){
        boolean next = true;
        if (username.getText().toString().isEmpty()){
            username.setError("Mohon Di Isi");
            next = false;
        }
        if (email.getText().toString().isEmpty()){
            email.setError("Mohon Di Isi");
            next = false;
        }
        if (phone.getText().toString().isEmpty()){
            phone.setError("Mohon Di Isi");
            next = false;
        }
        if (password.getText().toString().isEmpty()){
            password.setError("Mohon Di Isi");
            next = false;
        }
        if (confpassword.getText().toString().isEmpty()){
            confpassword.setError("Mohon Di Isi");
            next = false;
        }

        if (!password.getText().toString().equals(confpassword.getText().toString())){
            password.setError("Password Tidak Sama");
            confpassword.setError("Password Tidak Sama");
            next = false;
        }

        return next;
    }
}