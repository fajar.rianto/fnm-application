package com.example.fnm.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import com.example.fnm.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class EditProfilActivity extends AppCompatActivity {

    LinearLayout lin;
    LottieAnimationView lottieAnimationView;
    private EditText username;
    private EditText email;
    private EditText phone;
    private EditText kalori;
    private Spinner gender;
    private EditText bb;
    private EditText steps;
    private EditText oldpassword;
    private EditText password;
    private EditText confPassword;
    private Button edit;

    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        lin = findViewById(R.id.lin);
        lottieAnimationView = findViewById(R.id.lottie);

        username = findViewById(R.id.et_username);
        email = findViewById(R.id.et_email);
        phone = findViewById(R.id.et_phone);
        gender = findViewById(R.id.listItem);
        kalori = findViewById(R.id.et_kalori);
        bb = findViewById(R.id.et_bb);
        steps = findViewById(R.id.et_steps);
        edit = findViewById(R.id.btnEdit);
        oldpassword = findViewById(R.id.et_oldpassword);
        password = findViewById(R.id.et_password);
        confPassword = findViewById(R.id.et_confpassword);

        username.setText(getIntent().getStringExtra("username"));
        email.setText(getIntent().getStringExtra("email"));
        phone.setText(getIntent().getStringExtra("phone"));
        kalori.setText(getIntent().getStringExtra("kalori"));
        bb.setText(getIntent().getStringExtra("et_bb"));
        steps.setText(getIntent().getStringExtra("steps"));

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()){
                    lin.setVisibility(View.GONE);
                    lottieAnimationView.setVisibility(View.VISIBLE);
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("users")
                            .document(getIntent().getStringExtra("uid"))
                            .update(
                                    "username", username.getText().toString(),
                                    "phone", phone.getText().toString(),
                                    "gender", gender.getSelectedItem().toString()

                            );
                    if (!password.getText().toString().isEmpty()){
                        user = FirebaseAuth.getInstance().getCurrentUser();
                        AuthCredential credential = EmailAuthProvider.getCredential(email.getText().toString(),oldpassword.getText().toString());
                        lin.setVisibility(View.GONE);
                        lottieAnimationView.setVisibility(View.VISIBLE);

                        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    user.updatePassword(password.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(!task.isSuccessful()){
                                                Toast.makeText(getApplicationContext(), task.toString(), Toast.LENGTH_LONG).show();
                                            }else {
                                                Toast.makeText(getApplicationContext(), "Password Successfully Modified", Toast.LENGTH_LONG).show();
                                                Intent returnIntent = new Intent();
                                                returnIntent.putExtra("result","result");
                                                setResult(Activity.RESULT_OK,returnIntent);
                                                finish();
                                            }
                                        }
                                    });
                                }else {
//                            Snackbar snackbar_su = Snackbar
//                                    .make(coordinatorLayout, "Authentication Failed", Snackbar.LENGTH_LONG);
//                            snackbar_su.show();
                                    lin.setVisibility(View.VISIBLE);
                                    lottieAnimationView.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "Authentication Failed", Toast.LENGTH_LONG).show();

                                }
                            }
                        });
                        lin.setVisibility(View.VISIBLE);
                        lottieAnimationView.setVisibility(View.GONE);

                    }else{
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result","result");
                        setResult(Activity.RESULT_OK,returnIntent);
                        lin.setVisibility(View.VISIBLE);
                        lottieAnimationView.setVisibility(View.GONE);
                        finish();
                    }

                }
            }
        });

    }

    private boolean check() {
        boolean next = true;
        if (username.getText().toString().isEmpty()) {
            username.setError("Mohon Di Isi");
            next = false;
        }

        if (email.getText().toString().isEmpty()) {
            email.setError("Mohon Di Isi");
            next = false;
        }

        if (phone.getText().toString().isEmpty()) {
            phone.setError("Mohon Di Isi");
            next = false;
        }

        if (!password.getText().toString().equals(confPassword.getText().toString())){
            password.setError("Password Tidak Sama");
            confPassword.setError("Password Tidak Sama");
            next = false;
        }

        return next;
    }
}