package com.example.fnm.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.fnm.R;;

public class DetailTutorialActivity extends AppCompatActivity {

    TextView title, detail;
    String[] tutor = {"Crunch", "Jumping Jack", "Lunges", "Plank","Push Up","Squats"};
    int[] res = {R.raw.crunch, R.raw.jumpingjacks, R.raw.lunges, R.raw.plank, R.raw.pushup, R.raw.squats};
    String[] details = {
            "1.\tPosisikan tubuh Anda berbaring terlentang sambil menekuk lutut dan tempatkan telapak kaki menyentuh lantai.\n\n" +
                    "2.\tTempatkan lengan di dada secara bersila, lalu angkat tubuh hingga perut terasa kencang. Saat mengangkat tubuh, usahakan agar leher dan bahu tetap rileks.\n\n" +
                    "3.\tSetelah itu, tarik napas sambil kembali berbaring dan ulangi gerakan crunch hingga 10–15 kali.\n\n",
            "1.\tBerdiri tegak lurus dengan kaki menempel dan rapatkan tangan ke bawah pada sisi kanan dan kiri tubuh Anda.\n\n" +
                    "2.\tKemudian lompatkan kaki Anda ke samping kanan dan kiri hingga kaki terbuka lebar. Secara bersamaan, angkat tangan Anda ke atas kepala seperti bertepuk tangan.\n\n" +
                    "3.\tSegera kembali lagi ke posisi semula saat mendarat dan lakukan berulang-ulang.\n\n",
            "1.\tPertama-tama, taruh kedua tangan di pinggang dan berdiri tegak dengan bahu ditarik sedikit ke belakang.\n\n" +
                    "2.\tMulailah dengan langkahkan kaki kanan ke depan, lalu pelan-pelan turunkan tubuhmu sampai lutut kaki depan menekuk 90 derajat.\n\n" +
                    "3.\tTahan selama beberapa hitungan, lalu kembali ke posisi semula.\n\n",
            "1.\tSiapkan alas seperti karpet atau matras yang rata dan nyaman untuk tangan dan kaki.\n\n" +
                    "2.\tPosisikan tubuh seperti telungkup, tapi tubuh jangan sampai menempel dengan alas.\n\n" +
                    "3.\tAgar tubuh bisa “mengambang”, posisikan lengan menghadap ke depan dan tekuk siku hingga posisi 90 derajat.\n\n" +
                    "4.\tPosisi kaki lurus ke belakang sambil kaki bertumpu pada ujung-ujung jari kaki.\n\n" +
                    "5.\tPosisikan wajah menghadap ke bawah dan buat bahu berada pada posisi rileks.\n\n" +
                    "6.\tSetelah mendapatkan posisi plank yang baik dan benar, mulai latih otot dengan menahan bagian perut dan tarik area pusar ke arah dalam (perut seperti sedikit dikempiskan).\n\n" +
                    "7.\tPastikan posisi tubuh lurus dari ujung kepala hingga ujung kaki.\n\n" +
                    "8.\tTahan posisi tersebut selama 10 detik, lalu lepaskan.\n\n" +
                    "9.\tJika sudah berhasil melakukan gerakan plank selama 10 detik, dan selanjutnya, tambah durasi plank ke 30, 45, atau 60 detik.\n\n",
            "1.\tAmbil posisi merangkak di atas matras atau lantai. Posisikan kedua tangan Anda sedikit lebih lebar dari bahu.\n\n" +
                    "2.\tRentangkan kaki Anda ke belakang dan pastikan posisi tubuh tetap seimbang. Jagalah posisi tubuh tetap lurus dari kepala hingga kaki tanpa adanya lengkungan pada bagian punggung. Atur jarak kedua kaki (boleh lebar maupun rapat) untuk mendapatkan posisi yang nyaman.\n\n" +
                    "3.\tSebelum memulai gerakan, kontraksikan perut dan kencangkan otot inti tubuh dengan menarik bagian pusar ke arah tulang belakang. Pertahankan kondisi tersebut selama melakukan gerakan push up.\n\n" +
                    "4.\tTurunkan tubuh dengan cara menekuk siku hingga berada pada sudut 90 derajat. Saat menekuk siku, jangan lupa untuk menarik napas secara perlahan. \n\n" +
                    "5.\tTerakhir, buang napas ketika Anda mulai mengontraksikan otot dada dan menggunakan tangan untuk mendorong tubuh kembali ke posisi semula.\n\n",
            "1.\tAwali dengan posisi berdiri tegak.\n\n" +
                    "2.\tBerdiri dengan kaki dibuka selebar pinggul\n\n" +
                    "3.\tTurunkan tubuh Anda sejauh yang Anda bisa dengan mendorong punggung ke belakang, sambil naikkan lengan Anda lurus ke depan untuk menjaga keseimbangan.\n\n" +
                    "4.\tTubuh bagian bawah harus sejajar dengan lantai dan dada harus dibusungkan, tidak membungkuk. Lalu angkat sebentar dan kembali ke posisi awal.\n\n" +
                    "5.\tSaat Anda menurunkan tubuh Anda seperti ingin duduk atau jongkok, paha belakang memanjang di sendi pinggul dan memperpendek sendi lutut.\n\n" +
                    "6.\tPada saat yang sama, otot punggung atas menegang, yang membantu Anda mempertahankan batang tubuh Anda tetap tegak, sehingga punggung Anda tidak berputar.\n\n"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tutorial);

        ImageView btn_home = (ImageView)findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title = (TextView)findViewById(R.id.title);
        detail = (TextView)findViewById(R.id.detail);
        VideoView videoView = (VideoView)findViewById(R.id.video_view);

        int getId = getIntent().getIntExtra("tutorial",0);

        title.setText(tutor[getId]);
        detail.setText(details[getId]);
        String videPath = "android.resource://" + getPackageName() + "/" + res[getId];
        Uri uri = Uri.parse(videPath);
        videoView.setVideoURI(uri);

        MediaController mediaController = new MediaController(DetailTutorialActivity.this);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
    }
}