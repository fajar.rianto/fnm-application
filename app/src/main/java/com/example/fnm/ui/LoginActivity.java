package com.example.fnm.ui;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import com.example.fnm.MainActivity;
import com.example.fnm.R;
import com.example.fnm.tools.Constraint;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends AppCompatActivity {

    LinearLayout lin;
    LottieAnimationView lottieAnimationView;
    EditText username;
    EditText password;
    TextView register;
    Button btnLogin;
    DatabaseReference databaseReference;
    FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        databaseReference = FirebaseDatabase.getInstance().getReference("Users");
        auth = FirebaseAuth.getInstance();
        lin = findViewById(R.id.lin);
        lottieAnimationView = findViewById(R.id.lottie);
        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        register = (TextView)findViewById(R.id.register);
        btnLogin = (Button)findViewById(R.id.btnLogin);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()){
                    lin.setVisibility(View.GONE);
                    lottieAnimationView.setVisibility(View.VISIBLE);
                    auth.signInWithEmailAndPassword(username.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            lin.setVisibility(View.VISIBLE);
                            lottieAnimationView.setVisibility(View.GONE);
                            if (task.isSuccessful()){

                                FirebaseFirestore db = FirebaseFirestore.getInstance();
                                db.collection("users")
                                        .whereEqualTo("email", username.getText().toString())
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    SharedPreferences sharedPreferences;
                                                    sharedPreferences = getSharedPreferences("user_details", MODE_PRIVATE);
                                                    sharedPreferences.contains("email");

                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        Log.d(TAG, document.getId() + " => " + document.getData());
                                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                                        editor.putString("uid", document.getId());
                                                        editor.putString("email", document.getData().get("email").toString());
                                                        editor.apply();
                                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                        finish();
                                                    }
                                                } else {

                                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                                }
                                            }
                                        });


                            }else{
                                Toast.makeText(getApplicationContext(), Constraint.FAILEDLOGIN,Toast.LENGTH_LONG).show();
                            }

                            lin.setVisibility(View.VISIBLE);
                            lottieAnimationView.setVisibility(View.GONE);

                        }
                    });
                }
            }
        });
    }

    private boolean check(){
        boolean next = true;
        if (username.getText().toString().isEmpty()){
            username.setError("Mohon Di Isi");
            next = false;
        }
        if (password.getText().toString().isEmpty()){
            password.setError("Mohon Di Isi");
            next = false;
        }

        return next;
    }
}