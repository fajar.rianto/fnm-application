package com.example.fnm.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.example.fnm.MainActivity;
import com.example.fnm.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashActivity extends AppCompatActivity {


FirebaseUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getSharedPreferences("user_details", MODE_PRIVATE);

                if (sharedPreferences.getString("email",null) == null) {
                    Intent home = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(home);
                }else{
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
                finish();

            }
        },2000);

    }
}