package com.example.fnm;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class myapplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
    }
}
