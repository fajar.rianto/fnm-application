package com.example.fnm.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.fnm.R;
import com.example.fnm.database.dbManager;
import com.example.fnm.model.Model;
import com.example.fnm.ui.EditReminderActivity;

import java.util.ArrayList;

public class myAdapter extends RecyclerView.Adapter<myAdapter.myviewholder> {

    ArrayList<Model> dataholder = new ArrayList<Model>();                                               //array list to hold the reminders

    Context context;

    public interface PlayPauseClick {
        void imageButtonOnClick(View v, Intent intent);
    }

    private PlayPauseClick callback;

    public void setPlayPauseClickListener(PlayPauseClick listener) {
        this.callback = listener;
    }


    public myAdapter(ArrayList<Model> dataholder, Context context) {
        this.dataholder = dataholder;
        this.context = context;



    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_reminder_file, parent, false);  //inflates the xml file in recyclerview
        return new myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, @SuppressLint("RecyclerView") int position) {
        holder.mTitle.setText(dataholder.get(position).getTitle());                                 //Binds the single reminder objects to recycler view
        holder.mDate.setText(dataholder.get(position).getDate() + " " + dataholder.get(position).getTime());


        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditReminderActivity.class);
                intent.putExtra("mTitle", dataholder.get(position).getTitle());
                intent.putExtra("mDate", dataholder.get(position).getDate());
                intent.putExtra("mTime", dataholder.get(position).getTime());

//                Toast.makeText(context,dataholder.get(position).getDate(), Toast.LENGTH_LONG).show();

//                ((Activity) context).startActivityForResult(intent,202);

                if (callback != null) {
                    callback.imageButtonOnClick(v, intent);
                }
            }
        });

        holder.btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showDialog(dataholder.get(position).getTitle(), dataholder.get(position).getDate(), dataholder.get(position).getTime(), context, position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataholder.size();
    }

    class myviewholder extends RecyclerView.ViewHolder {

        TextView mTitle, mDate, mTime;
        LinearLayout lin;
        Button btndelete;

        public myviewholder(@NonNull View itemView) {
            super(itemView);

            lin = (LinearLayout)itemView.findViewById(R.id.lin);
            mTitle = (TextView) itemView.findViewById(R.id.txtTitle);                               //holds the reference of the materials to show data in recyclerview
            mDate = (TextView) itemView.findViewById(R.id.txtDate);
//            mTime = (TextView) itemView.findViewById(R.id.txtTime);
            btndelete = (Button) itemView.findViewById(R.id.btndelete);
        }
    }

    public void showDialog(final String title, String date, String time, Context context, int position) throws Exception {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage("Hapus : " + title + " ?");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){

                String result = new dbManager(context).deleteF(title, date, time);                  //inserts the title,date,time into sql lite database
                Toast.makeText(context, result,Toast.LENGTH_LONG).show();

                dataholder.remove(position);
                notifyItemRemoved(position);
                dialog.dismiss();

            }
        });

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.show();
    }
}